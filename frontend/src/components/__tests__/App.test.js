import {render, fireEvent, act} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom';
import App from '../../App';
import {getOnlyVisible, getManyVisible, getNotVisible} from './common';
import Top from '../topbar';
// const supertest = require('supertest');
// const db = require('../../../../backend/src/__tests__/db');
// const app = require('../../../../backend/src/app');
// const http = require('http');

// let server;
/**
 */

//  beforeAll(() => {
//   server = http.createServer(app);
//   server.listen();
//   // request = supertest(server);
//   return db.reset();
// });


test('App Renders', async () => {
  render(<App />);
  getOnlyVisible('All');
});

test('Check car', async () => {
  render(<App />);
  getManyVisible('Car');
});

test('click all', async () => {
  render(<App />);
  fireEvent.click(getOnlyVisible('All'));
});

test('click car', async () => {
  render(<App />);
  fireEvent.click(getManyVisible('Car'));
});

test('localstorage', async () => {
  if (localStorage.getItem('user') == null) {
    localStorage.setItem('user', '{"name":"Daniel","accessToken":"abc"}');
  }
  render(<Top />);
  // getOnlyVisible('Welcome Daniel');
  getOnlyVisible('log out');
  fireEvent.click(getOnlyVisible('log out'));
  getOnlyVisible('log in');

  // getOnlyVisible('LOG IN');
  // localStorage.removeItem('user');
});


test('login', async () => {
  // fetchMock.mock('/authenticate', 200);
  global.fetch = jest.fn(()=>{
    return new Promise((resolve, reject)=>{
      resolve({
        ok: true,
        json: () => ({
          'name': 'test',
          'accessToken': 'ass',
        }),
      });
    });
  });
  render(<Top />);
  fireEvent.click(getOnlyVisible('log in'));
  const a = document.getElementById('loginname');
  userEvent.type(a, 'abc');
  const b = document.getElementById('loginpwd');
  userEvent.type(b, 'pwd');
  await act(async ()=>{
    fireEvent.click(getOnlyVisible('LOGIN'));
  });
  getOnlyVisible('Welcome test');
  getOnlyVisible('log out');
  await act(async ()=>{
    fireEvent.click(getOnlyVisible('log out'));
  });
  console.log(localStorage.getItem('user'));
  getNotVisible('Welcome test');
  // getOnlyVisible('log in');
});

test('invalide login', async () => {
  window.alert = jest.fn();
  global.fetch = jest.fn(()=>{
    return new Promise((resolve, reject)=>{
      resolve({
        ok: false,
      });
    });
  });
  render(<Top />);
  fireEvent.click(getOnlyVisible('log in'));
  // await act(async ()=>{
  fireEvent.click(getOnlyVisible('LOGIN'));
  // expect(window.alert.mock.calls.length).toBe(1);
  // });
});

test('inv sign up', async () => {
  console.log(localStorage.getItem('user'));
  global.fetch = jest.fn(()=>{
    return new Promise((resolve, reject)=>{
      resolve({
        ok: false,
        // json: () => ({
        //   "name":"test",
        //   "accessToken":"ass"
        // })
      });
    });
  });
  render(<Top />);
  fireEvent.click(getOnlyVisible('sign up'));
  const a = document.getElementById('signupemail');
  userEvent.type(a, 'abc');
  const b = document.getElementById('signupname');
  userEvent.type(b, 'TEST');
  const c = document.getElementById('signuppwd');
  userEvent.type(c, 'abc');
  await act(async ()=>{
    fireEvent.click(getOnlyVisible('SIGN UP'));
  });
  getOnlyVisible('This email has already signed up, use login');
  fireEvent.click(getOnlyVisible('sign up'));
});

test('sign up', async () => {
  global.fetch = jest.fn(()=>{
    return new Promise((resolve, reject)=>{
      resolve({
        ok: true,
        json: () => ({
          'name': 'test',
          'accessToken': 'ass',
        }),
      });
    });
  });
  render(<Top />);
  fireEvent.click(getOnlyVisible('sign up'));
  const a = document.getElementById('signupemail');
  userEvent.type(a, 'abc');
  const b = document.getElementById('signupname');
  userEvent.type(b, 'TEST');
  const c = document.getElementById('signuppwd');
  userEvent.type(c, 'abc');
  await act(async ()=>{
    fireEvent.click(getOnlyVisible('SIGN UP'));
  });
  getOnlyVisible('Welcome test');
  await act(async ()=>{
    fireEvent.click(getOnlyVisible('log out'));
  });
});

test('empty sign up', async () => {
  console.log(localStorage.getItem('user'));
  window.alert = jest.fn();
  render(<Top />);
  fireEvent.click(getOnlyVisible('sign up'));
  // fireEvent.click(getOnlyVisible('log in'));
  const a = document.getElementById('signupemail');
  userEvent.type(a, '');
  const b = document.getElementById('signupname');
  userEvent.type(b, '');
  const c = document.getElementById('signuppwd');
  userEvent.type(c, '');
  await act(async ()=>{
    fireEvent.click(getOnlyVisible('SIGN UP'));
  });
  getOnlyVisible('Can not have empty input');
});
