import {render, fireEvent, act} from '@testing-library/react';
import '@testing-library/jest-dom';
import {getOnlyVisible, getClickable,
  getManyVisible, getAnyVisible, getNotVisible} from './common';
import Post from '../Post';
import Top from '../topbar';
import Sidebar from '../Sidebar';
import LoginNotice from '../LoginNotice';
import Categories from '../Categories';
import Actions from '../Actions';
import App from '../../App';

test('free post', async () => {
  render(<Post />);
  getOnlyVisible('FREE');
  fireEvent.click(getOnlyVisible('FREE'));
  fireEvent.click(getClickable('close'));
});

test('top bar', async () => {
  render(<Top />);
  getNotVisible('Your email address');
  fireEvent.click(getOnlyVisible('sign up'));
  getOnlyVisible('Sign Up');
  getOnlyVisible('Your email address');
  getOnlyVisible('Name');
  getOnlyVisible('Password');
  getNotVisible('Sign In');
  fireEvent.click(getOnlyVisible('log in'));
  getOnlyVisible('Sign In');
  getOnlyVisible('User Email');
  getOnlyVisible('Password');
});

test('Side bar', async () => {
  render(<Sidebar />);
  getOnlyVisible('Electronics');
  getOnlyVisible('Books');
  getOnlyVisible('Car');
  getOnlyVisible('House');
});

test('LoginNotice', async () => {
  render(<LoginNotice />);
  getOnlyVisible('Learn more');
});

// will cause error because no data is parsed into Feed.js
// test('Feed', async () => {
//     render(<Body />);
//     screen.debug();
//   });

test('click car', async () => {
  render(<Categories />);
  fireEvent.click(getManyVisible('Car'));
});

test('click estuff', async () => {
  render(<Categories />);
  fireEvent.click(getManyVisible('Electronics'));
});

test('click house', async () => {
  render(<Categories />);
  fireEvent.click(getManyVisible('House'));
});

test('click book', async () => {
  render(<Categories />);
  fireEvent.click(getManyVisible('Books'));
});


// test('Body', async () => {
//     render(<Body />);
//   });

test('Actions', async () => {
  render(<Actions />);
  getNotVisible('Select Categories');
  getOnlyVisible('All Categories');
  getNotVisible('Car');
  fireEvent.click(getOnlyVisible('All Categories'));
  getOnlyVisible('Car');
  fireEvent.click(getClickable('close'));
});

//  test('fetch car', async () => {
//  // fetchMock.mock('/authenticate', 200);
//  fetch = jest.fn(() => {
//    return new Promise((resolve, reject) => {
//      resolve({
//        ok: true,
//        json: () => ([
//          {
//            "dtype": "car",
//            "dvalue": [
//              {
//                "URL": "https://karltayloreducation.com/wp-content/uploads/2020/08/mercedes-car-3-4-1.jpg",
//                "price": "20000",
//                "Category": "bmwcar",
//                "userName": "zchu2@ucsc.edu",
//                "Date_Time": "10-2000",
//                "Description": "a car",
//                "productName": "bmw"
//              },
//              {
//                "URL": "https://cdn.motor1.com/images/mgl/JmVR6/s1/4x3/2019-audi-r8-onlocation.webp",
//                "price": "170000",
//                "Category": "audicar",
//                "userName": "jli563@ucsc.edu",
//                "Date_Time": "10-2021",
//                "Description": "a car",
//                "productName": "Audi"
//              },
//            ]
//          }
//          ])
//      });
//    });
//  });
//  await act(async () => {
//    render(<Actions />);
//    getNotVisible('Select Categories');
//    getOnlyVisible('All Categories');
//    getNotVisible('Car');
//    fireEvent.click(getOnlyVisible('All Categories'));
//    getOnlyVisible('Car');
//    fireEvent.click(getClickable('close'));
//  });
//  });

test('fetch car', async () => {
  // fetchMock.mock('/authenticate', 200);
  global.fetch = jest.fn(() => {
    return new Promise((resolve, reject) => {
      resolve({
        ok: true,
        json: () => ([
          {
            'dtype': 'car',
            'dvalue': [
              {
                'URL': 'https://karltayloreducation.com/wp-content/uploads/2020/08/mercedes-car-3-4-1.jpg',
                'price': '20000',
                'Category': 'bmwcar',
                'userName': 'zchu2@ucsc.edu',
                'Date_Time': '10-2000',
                'Description': 'a car',
                'productName': 'bmw',
              },
              {
                'URL': 'https://cdn.motor1.com/images/mgl/JmVR6/s1/4x3/2019-audi-r8-onlocation.webp',
                'price': '170000',
                'Category': 'audicar',
                'userName': 'jli563@ucsc.edu',
                'Date_Time': '10-2021',
                'Description': 'a car',
                'productName': 'Audi',
              },
            ],
          },
        ]),
      });
    });
  });
  await act(async () => {
    render(<App />);
    getNotVisible('Select Categories');
    getOnlyVisible('Car');
  });
  await act(async () => {
    fireEvent.click(getOnlyVisible('Car'));
  });
  getOnlyVisible('bmw');
});

test('fetch housing', async () => {
  // fetchMock.mock('/authenticate', 200);
  global.fetch = jest.fn(() => {
    return new Promise((resolve, reject) => {
      resolve({
        ok: true,
        json: () => ([
          {
            'dtype': 'housing',
            'dvalue': [
              {
                'URL': 'https://karltayloreducation.com/wp-content/uploads/2020/08/mercedes-car-3-4-1.jpg',
                'price': '20000',
                'Category': 'bmwcar',
                'userName': 'zchu2@ucsc.edu',
                'Date_Time': '10-2000',
                'Description': 'a car',
                'productName': 'bmw',
              },
              {
                'URL': 'https://cdn.motor1.com/images/mgl/JmVR6/s1/4x3/2019-audi-r8-onlocation.webp',
                'price': '170000',
                'Category': 'audicar',
                'userName': 'jli563@ucsc.edu',
                'Date_Time': '10-2021',
                'Description': 'a car',
                'productName': 'Audi',
              },
            ],
          },
        ]),
      });
    });
  });
  await act(async () => {
    render(<App />);
    getNotVisible('Select Categories');
    getOnlyVisible('House');
  });
  await act(async () => {
    fireEvent.click(getOnlyVisible('House'));
  });
  getOnlyVisible('bmw');
});
test('fetch estuff', async () => {
  // fetchMock.mock('/authenticate', 200);
  global.fetch = jest.fn(() => {
    return new Promise((resolve, reject) => {
      resolve({
        ok: true,
        json: () => ([
          {
            'dtype': 'estuff',
            'dvalue': [
              {
                'URL': 'https://karltayloreducation.com/wp-content/uploads/2020/08/mercedes-car-3-4-1.jpg',
                'price': '20000',
                'Category': 'bmwcar',
                'userName': 'zchu2@ucsc.edu',
                'Date_Time': '10-2000',
                'Description': 'a car',
                'productName': 'bmw',
              },
              {
                'URL': 'https://cdn.motor1.com/images/mgl/JmVR6/s1/4x3/2019-audi-r8-onlocation.webp',
                'price': '170000',
                'Category': 'audicar',
                'userName': 'jli563@ucsc.edu',
                'Date_Time': '10-2021',
                'Description': 'a car',
                'productName': 'Audi',
              },
            ],
          },
        ]),
      });
    });
  });
  await act(async () => {
    render(<App />);
    getNotVisible('Select Categories');
    getOnlyVisible('Electronics');
  });
  await act(async () => {
    fireEvent.click(getOnlyVisible('Electronics'));
  });
  getOnlyVisible('bmw');
});
test('fetch book', async () => {
  // fetchMock.mock('/authenticate', 200);
  global.fetch = jest.fn(() => {
    return new Promise((resolve, reject) => {
      resolve({
        ok: true,
        json: () => ([
          {
            'dtype': 'book',
            'dvalue': [
              {
                'URL': 'https://karltayloreducation.com/wp-content/uploads/2020/08/mercedes-car-3-4-1.jpg',
                'price': '20000',
                'Category': 'bmwcar',
                'userName': 'zchu2@ucsc.edu',
                'Date_Time': '10-2000',
                'Description': 'a car',
                'productName': 'bmw',
              },
              {
                'URL': 'https://cdn.motor1.com/images/mgl/JmVR6/s1/4x3/2019-audi-r8-onlocation.webp',
                'price': '170000',
                'Category': 'audicar',
                'userName': 'jli563@ucsc.edu',
                'Date_Time': '10-2021',
                'Description': 'a car',
                'productName': 'Audi',
              },
            ],
          },
        ]),
      });
    });
  });
  await act(async () => {
    render(<App />);
    getNotVisible('Select Categories');
    getOnlyVisible('Books');
  });
  await act(async () => {
    fireEvent.click(getOnlyVisible('Books'));
  });
  getOnlyVisible('bmw');
});
test('fetch all', async () => {
  // fetchMock.mock('/authenticate', 200);
  global.fetch = jest.fn(() => {
    return new Promise((resolve, reject) => {
      resolve({
        ok: true,
        json: () => ([
          {
            'dtype': 'book',
            'dvalue': [
              {
                'URL': 'https://karltayloreducation.com/wp-content/uploads/2020/08/mercedes-car-3-4-1.jpg',
                'price': '20000',
                'Category': 'bmwcar',
                'userName': 'zchu2@ucsc.edu',
                'Date_Time': '10-2000',
                'Description': 'a car',
                'productName': 'bmw',
              },
              {
                'URL': 'https://cdn.motor1.com/images/mgl/JmVR6/s1/4x3/2019-audi-r8-onlocation.webp',
                'price': '170000',
                'Category': 'audicar',
                'userName': 'jli563@ucsc.edu',
                'Date_Time': '10-2021',
                'Description': 'a car',
                'productName': 'Audi',
              },
            ],
          },
        ]),
      });
    });
  });
  await act(async () => {
    render(<App />);
    getNotVisible('Select Categories');
    getOnlyVisible('All');
  });
  await act(async () => {
    fireEvent.click(getOnlyVisible('All'));
  });
  getOnlyVisible('bmw');
});
test('fetch false', async () => {
  // fetchMock.mock('/authenticate', 200);
  global.fetch = jest.fn(() => {
    return new Promise((resolve, reject) => {
      resolve({
        ok: false,
        json: () => ([
          {
            'dtype': 'book',
            'dvalue': [
              {
                'URL': 'https://karltayloreducation.com/wp-content/uploads/2020/08/mercedes-car-3-4-1.jpg',
                'price': '20000',
                'Category': 'bmwcar',
                'userName': 'zchu2@ucsc.edu',
                'Date_Time': '10-2000',
                'Description': 'a car',
                'productName': 'bmw',
              },
              {
                'URL': 'https://cdn.motor1.com/images/mgl/JmVR6/s1/4x3/2019-audi-r8-onlocation.webp',
                'price': '170000',
                'Category': 'audicar',
                'userName': 'jli563@ucsc.edu',
                'Date_Time': '10-2021',
                'Description': 'a car',
                'productName': 'Audi',
              },
            ],
          },
        ]),
      });
    });
  });
  await act(async () => {
    render(<App />);
    getNotVisible('Select Categories');
    getOnlyVisible('All');
  });
  await act(async () => {
    fireEvent.click(getOnlyVisible('All'));
  });
});

test('fetch more than one', async () => {
  // fetchMock.mock('/authenticate', 200);
  global.fetch = jest.fn(() => {
    return new Promise((resolve, reject) => {
      resolve({
        ok: true,
        json: () => ([
          {
            'dtype': 'book',
            'dvalue': [
              {
                'URL': 'https://karltayloreducation.com/wp-content/uploads/2020/08/mercedes-car-3-4-1.jpg',
                'price': '20000',
                'Category': 'bmwcar',
                'userName': 'zchu2@ucsc.edu',
                'Date_Time': '10-2000',
                'Description': 'a car',
                'productName': 'bmw',
              },
              {
                'URL': 'https://cdn.motor1.com/images/mgl/JmVR6/s1/4x3/2019-audi-r8-onlocation.webp',
                'price': '170000',
                'Category': 'audicar',
                'userName': 'jli563@ucsc.edu',
                'Date_Time': '10-2021',
                'Description': 'a car',
                'productName': 'bmw',
              },
            ],
          },
        ]),
      });
    });
  });
  await act(async () => {
    render(<App />);
    getNotVisible('Select Categories');
    getOnlyVisible('All');
  });
  await act(async () => {
    fireEvent.click(getOnlyVisible('All'));
  });
  getManyVisible('bmw');
});

test('fetch all data', async () => {
  // fetchMock.mock('/authenticate', 200);
  global.fetch = jest.fn(() => {
    return new Promise((resolve, reject) => {
      resolve({
        ok: true,
        json: () => ([
          {
            'dtype': 'book',
            'dvalue': [
              {
                'URL': 'https://karltayloreducation.com/wp-content/uploads/2020/08/mercedes-car-3-4-1.jpg',
                'price': '20000',
                'Category': 'bmwcar',
                'userName': 'zchu2@ucsc.edu',
                'Date_Time': '10-2000',
                'Description': 'a car',
                'productName': 'bmw',
              },
              {
                'URL': 'https://cdn.motor1.com/images/mgl/JmVR6/s1/4x3/2019-audi-r8-onlocation.webp',
                'price': '170000',
                'Category': 'audicar',
                'userName': 'jli563@ucsc.edu',
                'Date_Time': '10-2021',
                'Description': 'a car',
                'productName': 'Audi',
              },
            ],
          },
          {
            'dtype': 'estuff',
            'dvalue': [
              {
                'URL': 'https://cdn.vox-cdn.com/thumbor/K-yujrt8GZ6ZtW6KrV2Pv6PphgI=/0x0:1024x683/1200x1200/filters:focal(512x342:513x343)/cdn.vox-cdn.com/uploads/chorus_asset/file/22017052/ps5_all_digital.jpg',
                'price': '2000',
                'Category': 'gaming console',
                'userName': 'zchu2@ucsc.edu',
                'Date_Time': '10-2000',
                'Description': 'a ps5',
                'productName': 'ps5',
              },
              {
                'URL': 'https://cdn.arstechnica.net/wp-content/uploads/2021/09/iPhone-13-Pro-Max-back.jpeg',
                'price': '17000',
                'Category': 'phone',
                'userName': 'jli563@ucsc.edu',
                'Date_Time': '10-2021',
                'Description': 'an iphone',
                'productName': 'iphone 13 proMax',
              },
            ],
          },
        ]),
      });
    });
  });
  await act(async () => {
    render(<App />);
    getNotVisible('Select Categories');
    getOnlyVisible('All');
  });
  await act(async () => {
    fireEvent.click(getOnlyVisible('All'));
  });
  getOnlyVisible('bmw');
});

test('fetch all second', async () => {
  // fetchMock.mock('/authenticate', 200);
  global.fetch = jest.fn(() => {
    return new Promise((resolve, reject) => {
      resolve({
        ok: true,
        json: () => ([
          {
            'dtype': 'car',
            'dvalue': [
              {
                'URL': 'https://karltayloreducation.com/wp-content/uploads/2020/08/mercedes-car-3-4-1.jpg',
                'price': '20000',
                'Category': 'bmwcar',
                'userName': 'zchu2@ucsc.edu',
                'Date_Time': '10-2000',
                'Description': 'a car',
                'productName': 'bmw',
              },
              {
                'URL': 'https://cdn.motor1.com/images/mgl/JmVR6/s1/4x3/2019-audi-r8-onlocation.webp',
                'price': '170000',
                'Category': 'audicar',
                'userName': 'jli563@ucsc.edu',
                'Date_Time': '10-2021',
                'Description': 'a car',
                'productName': 'Audi',
              },
            ],
          },
          {
            'dtype': 'estuff',
            'dvalue': [
              {
                'URL': 'https://cdn.vox-cdn.com/thumbor/K-yujrt8GZ6ZtW6KrV2Pv6PphgI=/0x0:1024x683/1200x1200/filters:focal(512x342:513x343)/cdn.vox-cdn.com/uploads/chorus_asset/file/22017052/ps5_all_digital.jpg',
                'price': '2000',
                'Category': 'gaming console',
                'userName': 'zchu2@ucsc.edu',
                'Date_Time': '10-2000',
                'Description': 'a ps5',
                'productName': 'ps5',
              },
              {
                'URL': 'https://cdn.arstechnica.net/wp-content/uploads/2021/09/iPhone-13-Pro-Max-back.jpeg',
                'price': '17000',
                'Category': 'phone',
                'userName': 'jli563@ucsc.edu',
                'Date_Time': '10-2021',
                'Description': 'an iphone',
                'productName': 'iphone 13 proMax',
              },
            ],
          },
        ]),
      });
    });
  });
  await act(async () => {
    render(<App />);
    getNotVisible('Select Categories');
    getOnlyVisible('Car');
  });
  await act(async () => {
    fireEvent.click(getOnlyVisible('Car'));
  });
  getOnlyVisible('bmw');
});
test('fetch more house', async () => {
  // fetchMock.mock('/authenticate', 200);
  global.fetch = jest.fn(() => {
    return new Promise((resolve, reject) => {
      resolve({
        ok: true,
        json: () => ([
          {
            'dtype': 'car',
            'dvalue': [
              {
                'URL': 'https://karltayloreducation.com/wp-content/uploads/2020/08/mercedes-car-3-4-1.jpg',
                'price': '20000',
                'Category': 'bmwcar',
                'userName': 'zchu2@ucsc.edu',
                'Date_Time': '10-2000',
                'Description': 'a car',
                'productName': 'bmw',
              },
              {
                'URL': 'https://cdn.motor1.com/images/mgl/JmVR6/s1/4x3/2019-audi-r8-onlocation.webp',
                'price': '170000',
                'Category': 'audicar',
                'userName': 'jli563@ucsc.edu',
                'Date_Time': '10-2021',
                'Description': 'a car',
                'productName': 'Audi',
              },
            ],
          },
          {
            'dtype': 'housing',
            'dvalue': [
              {
                'URL': 'https://cdn.vox-cdn.com/thumbor/K-yujrt8GZ6ZtW6KrV2Pv6PphgI=/0x0:1024x683/1200x1200/filters:focal(512x342:513x343)/cdn.vox-cdn.com/uploads/chorus_asset/file/22017052/ps5_all_digital.jpg',
                'price': '2000',
                'Category': 'gaming console',
                'userName': 'zchu2@ucsc.edu',
                'Date_Time': '10-2000',
                'Description': 'a ps5',
                'productName': 'ps5',
              },
              {
                'URL': 'https://cdn.arstechnica.net/wp-content/uploads/2021/09/iPhone-13-Pro-Max-back.jpeg',
                'price': '17000',
                'Category': 'phone',
                'userName': 'jli563@ucsc.edu',
                'Date_Time': '10-2021',
                'Description': 'an iphone',
                'productName': 'iphone 13 proMax',
              },
            ],
          },
        ]),
      });
    });
  });
  await act(async () => {
    render(<App />);
    getNotVisible('Select Categories');
    getOnlyVisible('House');
  });
  await act(async () => {
    fireEvent.click(getOnlyVisible('House'));
  });
  getOnlyVisible('ps5');
});
test('fetch all ele', async () => {
  // fetchMock.mock('/authenticate', 200);
  global.fetch = jest.fn(() => {
    return new Promise((resolve, reject) => {
      resolve({
        ok: true,
        json: () => ([
          {
            'dtype': 'car',
            'dvalue': [
              {
                'URL': 'https://karltayloreducation.com/wp-content/uploads/2020/08/mercedes-car-3-4-1.jpg',
                'price': '20000',
                'Category': 'bmwcar',
                'userName': 'zchu2@ucsc.edu',
                'Date_Time': '10-2000',
                'Description': 'a car',
                'productName': 'bmw',
              },
              {
                'URL': 'https://cdn.motor1.com/images/mgl/JmVR6/s1/4x3/2019-audi-r8-onlocation.webp',
                'price': '170000',
                'Category': 'audicar',
                'userName': 'jli563@ucsc.edu',
                'Date_Time': '10-2021',
                'Description': 'a car',
                'productName': 'Audi',
              },
            ],
          },
          {
            'dtype': 'estuff',
            'dvalue': [
              {
                'URL': 'https://cdn.vox-cdn.com/thumbor/K-yujrt8GZ6ZtW6KrV2Pv6PphgI=/0x0:1024x683/1200x1200/filters:focal(512x342:513x343)/cdn.vox-cdn.com/uploads/chorus_asset/file/22017052/ps5_all_digital.jpg',
                'price': '2000',
                'Category': 'gaming console',
                'userName': 'zchu2@ucsc.edu',
                'Date_Time': '10-2000',
                'Description': 'a ps5',
                'productName': 'ps5',
              },
              {
                'URL': 'https://cdn.arstechnica.net/wp-content/uploads/2021/09/iPhone-13-Pro-Max-back.jpeg',
                'price': '17000',
                'Category': 'phone',
                'userName': 'jli563@ucsc.edu',
                'Date_Time': '10-2021',
                'Description': 'an iphone',
                'productName': 'iphone 13 proMax',
              },
            ],
          },
        ]),
      });
    });
  });
  await act(async () => {
    render(<App />);
    getNotVisible('Select Categories');
    getOnlyVisible('Electronics');
  });
  await act(async () => {
    fireEvent.click(getOnlyVisible('Electronics'));
  });
  getAnyVisible('ps5');
});

test('fetch all many', async () => {
  // fetchMock.mock('/authenticate', 200);
  global.fetch = jest.fn(() => {
    return new Promise((resolve, reject) => {
      resolve({
        ok: true,
        json: () => ([
          {
            'dtype': 'car',
            'dvalue': [
              {
                'URL': 'https://karltayloreducation.com/wp-content/uploads/2020/08/mercedes-car-3-4-1.jpg',
                'price': '20000',
                'Category': 'bmwcar',
                'userName': 'zchu2@ucsc.edu',
                'Date_Time': '10-2000',
                'Description': 'a car',
                'productName': 'bmw',
              },
              {
                'URL': 'https://cdn.motor1.com/images/mgl/JmVR6/s1/4x3/2019-audi-r8-onlocation.webp',
                'price': '170000',
                'Category': 'audicar',
                'userName': 'jli563@ucsc.edu',
                'Date_Time': '10-2021',
                'Description': 'a car',
                'productName': 'Audi',
              },
            ],
          },
          {
            'dtype': 'book',
            'dvalue': [
              {
                'URL': 'https://cdn.vox-cdn.com/thumbor/K-yujrt8GZ6ZtW6KrV2Pv6PphgI=/0x0:1024x683/1200x1200/filters:focal(512x342:513x343)/cdn.vox-cdn.com/uploads/chorus_asset/file/22017052/ps5_all_digital.jpg',
                'price': '2000',
                'Category': 'gaming console',
                'userName': 'zchu2@ucsc.edu',
                'Date_Time': '10-2000',
                'Description': 'a ps5',
                'productName': 'ps5',
              },
              {
                'URL': 'https://cdn.arstechnica.net/wp-content/uploads/2021/09/iPhone-13-Pro-Max-back.jpeg',
                'price': '17000',
                'Category': 'phone',
                'userName': 'jli563@ucsc.edu',
                'Date_Time': '10-2021',
                'Description': 'an iphone',
                'productName': 'iphone 13 proMax',
              },
            ],
          },
        ]),
      });
    });
  });
  await act(async () => {
    render(<App />);
    getNotVisible('Select Categories');
    getOnlyVisible('Books');
  });
  await act(async () => {
    fireEvent.click(getOnlyVisible('Books'));
  });
  getManyVisible('ps5');
});
