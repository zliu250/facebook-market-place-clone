// login css accquired from https://codesandbox.io/s/animation-login-popup-form-forked-xobdm?file=/src/styles.css:0-2014
import React, {useState} from 'react';
import {AppBar, Button, Toolbar} from '@mui/material';
import logo from '../assets/images/Logo.png';
import {makeStyles} from '@material-ui/core/styles';
const styles = makeStyles({
  logo: {
    height: '55px',
    width: '100px',
  },
  toolbar: {
    backgroundColor: '#fff',
    display: 'flex',
    justifyContent: 'space-between',
  },
});
let hideSignup = true;
let show = false;
const Top = () => {
  const [signupEmg, setSignupEmg] = useState('');
  const [loginEmg, setLoginEmg] = useState('');
  let welcomemessage = '';
  const [refresh, setRefresh] = useState(0);
  const [user, setUser] =
    React.useState({username: '', name: '', password: ''});
  const [showact, setShowact] = useState(show);
  const classes = styles();
  const hasuser = localStorage.getItem('user') != null;
  const clickLog = () => {
    if (hasuser) {
      localStorage.removeItem('user');
      setShowact(!show);
    } else {
      hideSignup = true;
      show = !show;
      setShowact(show);
    }
  };

  const handleInputChange = (event) => {
    const {value, name} = event.target;
    const u = user;
    u[name] = value;
    setUser(u);
    setLoginEmg('');
    setSignupEmg('');
  };

  const onSubmit = (event) =>{
    console.log('calling submit');
    event.preventDefault();
    fetch('/authenticate', {
      method: 'POST',
      body: JSON.stringify(user),
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((res) => {
        console.log('at here!');
        console.log(res);
        if (!res.ok) {
          throw res;
        }
        return res.json();
      })
      .then((json) => {
        console.log(json);
        console.log(typeof(json));
        localStorage.setItem('user', JSON.stringify(json));
        show = false;
        setLoginEmg('');
        setShowact(false);
      })
      .catch((err) => {
        setLoginEmg('Error logging in, please try again');
        // alert('Error logging in, please try again');
      });
  };

  const onSignUp = (event) =>{
    event.preventDefault();
    if (user.username === '' || user.name === '' || user.password === '') {
      setSignupEmg('Can not have empty input');
      return;
    }
    fetch('/regester', {
      method: 'POST',
      body: JSON.stringify(user),
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((res) => {
        console.log(res);
        if (!res.ok) {
          throw res;
        }
        return res.json();
      })
      .then((json) => {
        console.log(typeof(json));
        localStorage.setItem('user', JSON.stringify(json));
        hideSignup = true;
        setRefresh(refresh+1);
        setShowact(false);
      })
      .catch((err) => {
        setSignupEmg('This email has already signed up, use login');
      });
  };

  const clickSignUp = (event) =>{
    hideSignup = !hideSignup;
    show = false;
    setRefresh(refresh+1);
    setShowact(false);
  };
  const u = localStorage.getItem('user');
  console.log(u);
  if (hasuser) {
    welcomemessage = 'Welcome ' + JSON.parse(u).name;
  }

  return (
    <div>
      <AppBar position='relative'>
        <Toolbar className={classes.toolbar} >
          <img src={logo} alt="Logo" className={classes.logo} />
          <div className="welcome">
            {welcomemessage}
          </div>
          <Button
            className={`${!hasuser ? '' : 'active'} show`}
            variant='contained' size='medium'
            onClick={clickSignUp}>
            sign up </Button>
          <Button
            variant='contained' size='medium'
            onClick={clickLog}>
            {hasuser ? 'log out':'log in'} </Button>
        </Toolbar>
      </AppBar>
      {/* <div className={`${showact ? '' : 'active'} show`}> */}
      <div hidden ={!showact}>
        <div className="login-form">
          <div className="form-box solid">
            <form onSubmit={onSubmit}>
              <h1 className="login-text">Sign In</h1>
              <label>User Email</label>
              <br></br>
              <input
                id = 'loginname'
                type="text"
                name="username"
                className="login-box"
                onChange={handleInputChange}
              />
              <br></br>
              <label>Password</label>
              <br></br>
              <input
                id = 'loginpwd'
                type="password"
                name="password"
                className="login-box"
                onChange={handleInputChange}
              />
              <br></br>
              <label> {loginEmg} </label>
              <input
                type="submit"
                value="LOGIN"
                className="login-btn"/>
            </form>
          </div>
        </div>
      </div>
      <div hidden ={hideSignup}>
        <div className="login-form">
          <div className="form-box solid">
            <form onSubmit={onSignUp}>
              <h1 className="login-text">Sign Up</h1>
              <label>Your email address</label>
              <br></br>
              <input
                id = 'signupemail'
                type="text"
                name="username"
                className="login-box"
                onChange={handleInputChange}
              />
              <br></br>
              <label>Name</label>
              <br></br>
              <input
                id = 'signupname'
                type="text"
                name="name"
                className="login-box"
                onChange={handleInputChange}
              />
              <br></br>
              <label>Password</label>
              <br></br>
              <input
                id = 'signuppwd'
                type="password"
                name="password"
                className="login-box"
                onChange={handleInputChange}
              />
              <br></br>
              <label> {signupEmg} </label>
              <input
                type="submit"
                value="SIGN UP"
                className="login-btn"/>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Top;
